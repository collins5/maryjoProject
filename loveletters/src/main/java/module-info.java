module client.loveletters {
    requires javafx.controls;
    requires javafx.fxml;
    requires com.fasterxml.jackson.databind;

    opens protokoll to com.fasterxml.jackson.databind;
    exports protokoll;

    opens client to javafx.fxml;
    exports client;
}