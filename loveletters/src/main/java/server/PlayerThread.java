package server;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import protokoll.Message;

import java.io.*;
import java.net.Socket;
import java.util.*;
import java.util.ArrayList;

public class PlayerThread extends Thread {

    private final Socket client_socket;
    private final Server server;
    private final OutputStream outputStream;
    private final InputStream inputStream;
    private final PrintStream output;
    private final BufferedReader input;
    private final ObjectMapper objectMapper = new ObjectMapper();
    static List<PlayerThread> THREADS = new ArrayList<>();
    private String playerName;

    public PlayerThread(Socket client_socket, Server server) {
        this.client_socket = client_socket;
        this.server = server;
        THREADS.add(this);

        try {
            outputStream = client_socket.getOutputStream();
            output = new PrintStream(outputStream, true);

            inputStream = client_socket.getInputStream();
            input = new BufferedReader(new InputStreamReader(inputStream));

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void run() {
        String message = "";
        while (client_socket.isConnected() && null != message) {

            try {

                while((message = input.readLine())!= null)    {

                    Message userMessage = objectMapper.readValue(message, Message.class);
                    this.setPlayerName(userMessage.getFrom());
                    switch (userMessage.getType()) {
                        case CONNECT -> server.verifyConnection(this);
                        case DISCONNECT -> server.closeConnection(this);
                        case CHATTING -> {
                            if (Objects.equals(userMessage.getContent(), "bye")) {
                                server.closeConnection(this);
                            }
                            this.server.sendToAllPlayers(userMessage);
                        }
                        default -> {
                        }
                    }
                }

            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        this.closeConnection();

    }

    public void sendMessage(String message) {
        if (client_socket.isConnected()) {
            output.println(message);
        }
    }

    public void sendMessage(Message message) {
        ObjectMapper mapper = new ObjectMapper();
        String messageString;

        try {
            messageString = mapper.writeValueAsString(message);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

        sendMessage(messageString);
    }

    public void closeConnection() {
        try {
            outputStream.close();
            output.close();
            inputStream.close();
            input.close();
            client_socket.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void  setPlayerName(String playerName){
        this.playerName= playerName;
    }

    public String getPlayerName(){
        return this.playerName;
    } 
    public Socket getSocket(){
        return this.client_socket;
    }
}
