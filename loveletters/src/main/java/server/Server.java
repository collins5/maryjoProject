package server;

import java.net.ServerSocket;
import java.util.ArrayList;
import java.io.IOException;
import java.net.Socket;
import protokoll.Message;
import protokoll.Messagetype;

/*this class  establish connection 
between server and client and helps in communication between different users.
*/ 
public class Server{
    private final int port;
    public static final ArrayList<PlayerThread> players = new ArrayList<>(4);

    public ArrayList<PlayerThread> getPlayerThreads(){
        return players;
    }
    public Server(int port){
        this.port = port;
    }
    public static void main(String[] args){
        int port = 6662;
        Server server = new Server(port);
        server.start();
    }
// start() methods opens a channel for the connection between Server and Client
    private void start() {
        try{
            ServerSocket serverSocket = new ServerSocket(port);
            acceptClients(serverSocket);
        }
        catch (IOException e){
        }
    } 
     //this method accepts the clients request and server assign a seperate thread to handle multiple clients
    public void acceptClients(ServerSocket serverSocket) throws IOException{
        boolean accept = true;

        while (accept){
            try{
                Socket client_socket = serverSocket.accept();
                PlayerThread thread = new PlayerThread(client_socket, this);
                players.add(thread);
                thread.start();
            }
            catch(IOException e){
                accept = false;

            }
        }
    }
/* this method checks if the player has used 
same name or not, if not server start connection 
*/
    public void verifyConnection(PlayerThread playerThread) throws IOException {

        boolean found = false;
        for(PlayerThread player: players){
            if(player.getPlayerName().equals(playerThread.getPlayerName()) && player != playerThread) {
                found = true;
                break;
            }
        }
        if(!found) {
            Message successfeedback = new Message();
            successfeedback.setContent(playerThread.getPlayerName() + " login is successfull Welcome " +playerThread.getPlayerName()+ "." );
            successfeedback.setType(Messagetype.LOGINSUCCESS);
            playerThread.sendMessage(successfeedback);
            Message greetings = new Message();
            greetings.setType(Messagetype.CHATTING);
            greetings.setFrom("system");
            greetings.setContent( playerThread.getPlayerName() +" joined the Room.");
            sendToAllPlayers(greetings, playerThread.getPlayerName());
            Message greetingSelf = new Message();
            greetingSelf.setType(Messagetype.CHATTING);
            greetingSelf.setFrom("system");
            greetingSelf.setContent( "Welcome " + playerThread.getPlayerName() + ".");
            playerThread.sendMessage(greetingSelf);
        }
        else {
            players.remove(playerThread);
            Message failfeedback = new Message();
            failfeedback.setType(Messagetype.LOGINFAIL);
            failfeedback.setContent("the player " + playerThread.getPlayerName() + " login is Failed.");
            playerThread.sendMessage(failfeedback);
        }
    }
    
    public void sendToAllPlayers(Message message) throws IOException{
        this.sendToAllPlayers(message, null);
    }

    public void sendToAllPlayers(Message message, String exceptUser) {
        for(PlayerThread player : players){
            if(null == exceptUser || !exceptUser.equals(player.getPlayerName())) {
                player.sendMessage(message);
            }
        }
    }
//it send message if the player left the room  and close the connection
    public void closeConnection(PlayerThread playerThread) throws IOException {
        players.remove(playerThread);
        protokoll.Message logoutannoucement = new Message();
        logoutannoucement.setType(Messagetype.CHATTING);
        logoutannoucement.setFrom("System");
        logoutannoucement.setContent(playerThread.getPlayerName() + " Left the room.");
        sendToAllPlayers(logoutannoucement);
        protokoll.Message dc = new Message();
        dc.setType(Messagetype.LOGOUT);
        dc.setFrom(playerThread.getPlayerName());
        playerThread.sendMessage(dc);
        playerThread.closeConnection();
    }

}
