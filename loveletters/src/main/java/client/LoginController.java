package client;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.application.Platform;

import protokoll.Message;


public class LoginController{


    //damit man methode von logincontroller ausserhalb logincontroller zugreifen kann
    private static LoginController instance;
    public LoginController() {
        instance = this;
    }
    public static LoginController getInstance(){
        return instance;
    }

    @FXML
    private Label loginTextfield;
    @FXML
    private TextField usernamefield;
    @FXML
    private TextField hostnamefield;
    @FXML
    private TextField portfield;

    NetworkClient networkClient;
    String username;

    @FXML
    private TextArea chatverlauf;
    @FXML
    private TextField inputchattingtext;



    public void setNetworkClient(NetworkClient networkClient) {
        this.networkClient = networkClient;
        username = networkClient.getUserName();
    }


    //Messages wird auf Chatverlauf angezeigt mit Zeilenumbruch
    public void addMessges(Message message){
        chatverlauf.appendText(message.getFrom() + ": " + message.getContent());
        chatverlauf.appendText("\n");
    }



    //Loginfunktion welche sich mit Buttonclick thread startet
    @FXML
    protected void onLoginButtonClick() {
        if (!usernamefield.getText().isBlank() && !hostnamefield.getText().isBlank() && !portfield.getText().isBlank()){
            String username = usernamefield.getText().trim();
            String hostname = hostnamefield.getText().trim();
            int port = Integer.parseInt(portfield.getText().trim());
            this.networkClient = new NetworkClient(hostname, port, this,username);
            this.networkClient.setUserName(username);
            new Thread(networkClient).start();
            this.networkClient.login();
        }
        else {
            loginTextfield.setText("please enter username, hostname and port");
        }
    }
    public void setloginTextfield(String feedback){
        Platform.runLater(() -> {
            loginTextfield.setText(feedback);
        });
    }

    @FXML
    protected void onsendButtonClick() {
        String content = inputchattingtext.getText();
        if(!content.isEmpty()){
            networkClient.chat(content);
        }
    }
}