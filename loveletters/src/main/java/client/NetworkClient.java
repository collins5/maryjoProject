package client;

import com.fasterxml.jackson.core.JsonProcessingException;
import protokoll.Message;

import java.io.*;
import java.net.Socket;

import com.fasterxml.jackson.databind.ObjectMapper;
import protokoll.Messagetype;

import static protokoll.Messagetype.*;

public class NetworkClient extends Thread {

    private Socket socket;
    private OutputStream outputStream;
    private InputStream inputStream;
    private PrintStream output;
    private BufferedReader input;
    private String userName;
    private String server;
    private int port;
    private final LoginController loginController;

    // Socket Verbindung mit in- und output zum Server
    public NetworkClient(String server, int port, LoginController loginController, String username) {

        this.server = server;
        this.port = port;
        this.userName = username;
        this.loginController = loginController;

        try {
            socket = new Socket(server, port);

            outputStream = socket.getOutputStream();
            output = new PrintStream(outputStream, true);

            inputStream = socket.getInputStream();
            input = new BufferedReader(new InputStreamReader(inputStream));

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    //asyncrone Nachrichten (Nachrichten Thread) Message wird in den input eingelesen und den Kontroller zugeordent.
    public void run() {

        try {
            boolean isClosed = false;
            while (!isClosed && socket.isConnected()) {
                String message = input.readLine();
                if (message != null) {
                    Message newMessage = new ObjectMapper().readValue(message, Message.class);
                    switch (newMessage.getType()) {
                        case LOGINSUCCESS, LOGINFAIL -> loginController.setloginTextfield(newMessage.getContent());
                        case CHATTING -> loginController.addMessges(newMessage);
                        case LOGOUT -> {
                            closeConnection();
                            isClosed = true;
                        }
                    }
                }

            }
            this.closeConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //login wird zum Server gesendet
    public void login() {
        Message message = new Message();
        message.setType(Messagetype.CONNECT);
        message.setFrom(userName);
        sendMessage(message);
    }

    //wenn Verbunden, Message wird zum Server gesendet.
    public void sendMessage(String message) {
        if (socket.isConnected()) {
            output.println(message);
        }
    }

    //sendet eine Chat Message.
    public void chat(String message) {
        Message msg = new Message();
        msg.setContent(message);
        msg.setFrom(this.userName);
        msg.setType(Messagetype.CHATTING);
        this.sendMessage(msg);
    }

    // Die Message wird zu einem String umgewandet mittels JSON.
    public void sendMessage(Message message) {
        ObjectMapper mapper = new ObjectMapper();
        String messageString;

        try {
            messageString = mapper.writeValueAsString(message);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

        sendMessage(messageString);
    }

    //alle Verbinungen werden geschlossen.
    public void closeConnection() {
        try {
            outputStream.close();
            output.close();
            inputStream.close();
            input.close();
            socket.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
