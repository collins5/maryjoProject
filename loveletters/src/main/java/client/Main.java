package client;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class Main extends Application {
    @Override
    public void start(Stage stage) {
        Runnable task = () -> {
            Platform.runLater(() -> {
                Parent root = null;
                try {
                    root = FXMLLoader.load(getClass().getResource("Login.fxml"));
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                Scene scene = new Scene(root, 1000, 489);
                stage.setTitle("Loveletter");
                stage.setScene(scene);
                stage.show();
            });
        };
        Thread thread = new Thread(task);
        thread.setDaemon(true);
        thread.start();
    }


    public static void main(String[] args) {
        launch();
    }
}