package protokoll;


public class Message {
    /** Ein Message beansprucht drei Attributen:
     * type: was schliesslich dieses Message tut
     * from: von welchem User dieses kommt
     * content: Inhalt
     */
    private Messagetype type;
    private String from;
    private String content;

    public void setType(Messagetype type) {
        this.type = type;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Messagetype getType() {
        return type;
    }

    public String getFrom() {
        return from;
    }

    public String getContent() {
        return content;
    }
}
